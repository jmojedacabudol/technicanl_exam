const express = require('express');
const bodyParser = require('body-parser');
const mysql = require("mysql");

const app = express();

app.set('view engine', 'ejs');

const port = process.env.PORT || 3000;

app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(bodyParser.json())
//MYSQL

const pool = mysql.createPool({
    connectionLimit: 10,
    host: 'us-cdbr-east-03.cleardb.com',
    user: 'b3cc462c8894d6',
    password: 'e4d930f7',
    database: 'heroku_6262af9eb773fe8'
});


//get all books

app.get("/books", function (req, res) {

    pool.getConnection((err, connection) => {
        if (err) {
            if (err.code === 'PROTOCOL_CONNECTION_LOST') {
                console.error('Database connection was closed.')
            };
            if (err.code === 'ER_CON_COUNT_ERROR') {
                console.error('Database has too many connections.')
            };
            if (err.code === 'ECONNREFUSED') {
                console.error('Database connection was refused.')
            };
        };

        if (connection) {
            connection.query('SELECT * from Book', (err, rows) => {
                connection.release() //return to the connection pool

                if (!err) {
                    res.render("books", {
                        title: "Available Books",
                        items: rows
                    });
                } else {
                    console.log(err);
                }
            });
        };
    });
});



//select a book
app.get("/books/:id", function (req, res) {

    pool.getConnection((err, connection) => {
        if (err) {
            if (err.code === 'PROTOCOL_CONNECTION_LOST') {
                console.error('Database connection was closed.')
            };
            if (err.code === 'ER_CON_COUNT_ERROR') {
                console.error('Database has too many connections.')
            };
            if (err.code === 'ECONNREFUSED') {
                console.error('Database connection was refused.')
            };
        };

        if (connection) {
            connection.query('SELECT * from Book WHERE id=?', [req.params.id], (err, rows) => {
                connection.release() //return to the connection pool

                if (!err) {
                    res.send(rows);
                } else {
                    console.log(err);
                }
            });
        };
    });
});


//delete a book
app.get("/books/delete/:id", function (req, res) {

    pool.getConnection((err, connection) => {
        if (err) {
            if (err.code === 'PROTOCOL_CONNECTION_LOST') {
                console.error('Database connection was closed.')
            };
            if (err.code === 'ER_CON_COUNT_ERROR') {
                console.error('Database has too many connections.')
            };
            if (err.code === 'ECONNREFUSED') {
                console.error('Database connection was refused.')
            };
        };

        if (connection) {
            connection.query('DELETE from Book WHERE id=?', [req.params.id], (err, rows) => {
                connection.release() //return to the connection pool

                if (!err) {
                    res.send(`Book with ID record of ${req.params.id} has been removed. <a href="/books" class="btn btn-primary">Back</a>`);
                } else {
                    console.log(err);
                }
            });
        };
    });
});



//update Book
app.post("/books/update", function (req, res) {

    pool.getConnection((err, connection) => {
        if (err) {
            if (err.code === 'PROTOCOL_CONNECTION_LOST') {
                console.error('Database connection was closed.')
            };
            if (err.code === 'ER_CON_COUNT_ERROR') {
                console.error('Database has too many connections.')
            };
            if (err.code === 'ECONNREFUSED') {
                console.error('Database connection was refused.')
            };
        };

        if (connection) {
            var bookName = req.body.eBookName;
            var author = req.body.eBookAuthor;
            var publisher = req.body.eBookPublisher;
            var bookId = req.body.bookId;

            connection.query('UPDATE Book SET bookName=?, author=?,publisher=? WHERE id=?', [bookName, author, publisher, bookId], (err, rows) => {
                connection.release() //return to the connection pool

                if (!err) {
                    console.log(bookName, author, publisher, bookId);
                    res.send(`Book with name of ${bookName} has been Updated. <a href="/books" class="btn btn-primary">Back</a>`);
                } else {
                    console.log(err);
                }
            });
        };
    });
});


//add a book
app.post("/books", function (req, res) {

    pool.getConnection((err, connection) => {
        if (err) {
            if (err.code === 'PROTOCOL_CONNECTION_LOST') {
                console.error('Database connection was closed.')
            };
            if (err.code === 'ER_CON_COUNT_ERROR') {
                console.error('Database has too many connections.')
            };
            if (err.code === 'ECONNREFUSED') {
                console.error('Database connection was refused.')
            };
        };

        if (connection) {

            var book = {
                bookName: req.body.bookName,
                author: req.body.bookAuthor,
                publisher: req.body.bookPublisher
            };


            connection.query('INSERT INTO Book SET ?', book, (err, rows) => {
                connection.release() //return to the connection pool

                if (!err) {
                    res.send(`Book with name of ${book.bookName} has been added. <a href="/books" class="btn btn-primary">Back</a>`);
                } else {
                    console.log(err);
                }
            });
        };
    });
});



//get all students

app.get("/students", function (req, res) {

    pool.getConnection((err, connection) => {
        if (err) {
            if (err.code === 'PROTOCOL_CONNECTION_LOST') {
                console.error('Database connection was closed.')
            };
            if (err.code === 'ER_CON_COUNT_ERROR') {
                console.error('Database has too many connections.')
            };
            if (err.code === 'ECONNREFUSED') {
                console.error('Database connection was refused.')
            };
        };

        if (connection) {
            connection.query('SELECT * from students', (err, rows) => {
                connection.release() //return to the connection pool

                if (!err) {
                    res.render("students", {
                        title: "All Students",
                        items: rows
                    });
                } else {
                    console.log(err);
                }
            })
        };
    });
});


//select a student
app.get("/students/:id", function (req, res) {

    pool.getConnection((err, connection) => {
        if (err) {
            if (err.code === 'PROTOCOL_CONNECTION_LOST') {
                console.error('Database connection was closed.')
            };
            if (err.code === 'ER_CON_COUNT_ERROR') {
                console.error('Database has too many connections.')
            };
            if (err.code === 'ECONNREFUSED') {
                console.error('Database connection was refused.')
            };
        };

        if (connection) {
            connection.query('SELECT * from students WHERE id=?', [req.params.id], (err, rows) => {
                connection.release() //return to the connection pool

                if (!err) {
                    res.send(rows);
                } else {
                    console.log(err);
                }
            });
        };
    });
});


//delete a student
app.get("/students/delete/:id", function (req, res) {

    pool.getConnection((err, connection) => {
        if (err) {
            if (err.code === 'PROTOCOL_CONNECTION_LOST') {
                console.error('Database connection was closed.')
            };
            if (err.code === 'ER_CON_COUNT_ERROR') {
                console.error('Database has too many connections.')
            };
            if (err.code === 'ECONNREFUSED') {
                console.error('Database connection was refused.')
            };
        };

        if (connection) {
            connection.query('DELETE from students WHERE id=?', [req.params.id], (err, rows) => {
                connection.release() //return to the connection pool

                if (!err) {
                    res.send(`Student with ID record of ${req.params.id} has been removed. <a href="/students" class="btn btn-primary">Back</a>`);
                } else {
                    console.log(err);
                }
            });
        };
    });
});



//add a student
app.post("/students", function (req, res) {

    pool.getConnection((err, connection) => {
        if (err) {
            if (err.code === 'PROTOCOL_CONNECTION_LOST') {
                console.error('Database connection was closed.')
            };
            if (err.code === 'ER_CON_COUNT_ERROR') {
                console.error('Database has too many connections.')
            };
            if (err.code === 'ECONNREFUSED') {
                console.error('Database connection was refused.')
            };
        };

        if (connection) {
            var student = {
                student_No: req.body.studentNo,
                firstName: req.body.fName,
                lastName: req.body.lName
            };
            connection.query('INSERT INTO students SET ?', student, (err, rows) => {
                connection.release() //return to the connection pool

                if (!err) {
                    res.send(`Student with student No. of ${student.student_No} has been added. <a href="/students" class="btn btn-primary">Back</a>`);
                } else {
                    console.log(err);
                }
            });
        };
    });
});




//update student
app.post("/students/update", function (req, res) {

    pool.getConnection((err, connection) => {
        if (err) {
            if (err.code === 'PROTOCOL_CONNECTION_LOST') {
                console.error('Database connection was closed.')
            };
            if (err.code === 'ER_CON_COUNT_ERROR') {
                console.error('Database has too many connections.')
            };
            if (err.code === 'ECONNREFUSED') {
                console.error('Database connection was refused.')
            };
        };

        if (connection) {
            var eStudentNo = req.body.eStudentNo;
            var eFName = req.body.eFName;
            var eLName = req.body.eLName;
            var studentId = req.body.studentId;

            connection.query('UPDATE students SET student_No=?, firstName=?,lastName=? WHERE id=?', [eStudentNo, eFName, eLName, studentId], (err, rows) => {
                connection.release() //return to the connection pool

                if (!err) {
                    res.send(`Student with Student id of ${studentId} has been Updated. <a href="/students" class="btn btn-primary">Back</a>`);
                } else {
                    console.log(err);
                }
            });
        };
    });
});


app.get("/", function (req, res) {
    res.sendFile(__dirname + "/index.html");
});



app.get("/about", function (req, res) {
    res.send("This Project is created by John Michael");

});


app.listen(port, function () {
    console.log(`Server started on port ${port}`);
});